const Excel = require('exceljs');
let file = "final_data.xlsx";
let startIndex = 0;
//todo map all
let trueMap = {
    1: ["kass", "kasd", "loom", "tiigti", "tiigr", "kiisu", "lõvi", "kutsik"],
    2: ["lill", "roos", "kellu", "kress", "priimu", "gramm"],
    3: ["konn", "sisalik"],
    4: ["kahv"],
    5: ["orav", "loom", "hiir", "näril"],
    6: ["kann", "pott", "tee", "vee"],
    7: ["lill", "roos", "kellu", "kress", "priimu", "gramm"],
    8: ["hüljes", "vigri", "loom", "morsk", "vees elav imetaja"],
    9: ["laev", "paat", "purjekas"],
    10: ["tennis", "kets", "toss", "koss"],
    11: ["lind", "linnu", "ronk", "vares", "tuvi", "tihane", "varblane"],
    12: ["kahur", "tank", "sõja", "yank"],
    13: ["liblik"],
    14: ["konn", "loom"],
    15: ["elevant", "loom"],
    16: ["eesli", "eesel", "hobu", "lehm", "härj", "härg", "loom"],
};


let checkAnswer = (answer, colNum) => {
    if (answer) {
        let excelValue = ("" + answer).toLowerCase();
        if (excelValue.indexOf("ei tea") > -1) return 0;
        for (let checkValue of trueMap[colNum]) {
            if (excelValue.indexOf(checkValue) > -1) return 1;
        }
        for (let checkValue of ["ei tea", "eitea", "ei"]) {
            if (excelValue.indexOf(checkValue) > -1) return 0;
        }
    }
    return answer;
};

let workbook = new Excel.Workbook();
workbook.xlsx.readFile(file
).then(() => {
    let sheet1 = workbook.getWorksheet(1);
    //add answer columns
    let newRows = [];
    let row = sheet1.getRow(1);
    let delta = 112;
    for (let i= 0; i < 16; i++){
        let idx = (i*8) + 1 + delta;
        console.log(idx);
        sheet1.eachRow((row, rowNum) => {
            if (rowNum == 1) {
                row.splice(idx + 4, 0, row.getCell(idx+3).value +"_validated");
                row.splice(idx + 6, 0, row.getCell(idx+5).value +"_duration")
            } else {
                let answerValue = row.getCell(idx+3).value;
                let newAns = checkAnswer(answerValue, i+1);
                row.splice(idx + 4, 0, [0,1].indexOf(newAns) > -1 ? newAns : "");

                let startDate = row.getCell(idx).value;
                let endDate = row.getCell(idx + 5).value;
                console.log(startDate, endDate);
                let diff = "";
                if (endDate && startDate) {
                    diff = (endDate.getTime() - startDate.getTime())/1000;
                }
                row.splice(idx + 6, 0, "" + diff)
            }
        });
    }
    /*for (let key of Object.keys(trueMap)) {
        sheet1.eachRow((row, rowNum) => {
            let idx = +key + 1 + delta;
            console.log(idx);
            // if (rowNum == 1) {
            //     row.splice(idx, 0, row.getCell(idx+4).value +"_validated");
            // }
            return;
            /!*if (rowNum > 1) {
                let answerValue = row.getCell(idx+3).value;
                let newAns = checkAnswer(answerValue, key);
                row.splice(idx, 0, [0,1].indexOf(newAns) > -1 ? newAns : "");
            }

            //calculate times
            if (rowNum == 1) row.getCell(idx).value = row.getCell(idx).value.replace("_end", "_duration_sec");
            if (rowNum > 1) {
                let startDate = row.getCell(idx - 6).value;
                let endDate = row.getCell(idx -3).value;
                if (endDate && startDate) {
                    let diff = (endDate.getTime() - startDate.getTime())/1000;
                    row.getCell(idx).type = Excel.ValueType.Number;
                    row.getCell(idx).value = "" + diff;
                }
            }*!/
        });
    }*/
    workbook.xlsx.writeFile(file.replace(".", "_checked."));
});



